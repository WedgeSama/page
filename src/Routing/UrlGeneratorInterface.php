<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Routing;

use WS\Library\Page\Exception\Routing\TranslationNotFoundForLocaleException;
use WS\Library\Page\Model\PageInterface;

/**
 * Interface UrlGeneratorInterface
 *
 * @author Benjamin Georgeault
 * @see \Symfony\Component\Routing\Generator\UrlGeneratorInterface
 */
interface UrlGeneratorInterface
{
    // Use same constants as Symfony\Component\Routing\Generator\UrlGeneratorInterface
    public const int ABSOLUTE_URL = 0;
    public const int ABSOLUTE_PATH = 1;
    public const int RELATIVE_PATH = 2;
    public const int NETWORK_PATH = 3;

    /**
     * @throws TranslationNotFoundForLocaleException If cannot find Translation for the given locale and page.
     */
    public function generate(PageInterface $page, ?string $locale = null, array $parameters = [], int $referenceType = self::ABSOLUTE_PATH): string;
}

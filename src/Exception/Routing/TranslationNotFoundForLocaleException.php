<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Exception\Routing;

use WS\Library\Page\Exception\ExceptionInterface;
use WS\Library\Page\Model\PageInterface;

/**
 * Class TranslationNotFoundForLocaleException
 *
 * @author Benjamin Georgeault
 */
class TranslationNotFoundForLocaleException extends \RuntimeException implements ExceptionInterface
{
    public function __construct(
        protected readonly PageInterface $page,
        protected readonly string $locale,
    ) {
        parent::__construct(sprintf('Translation not found for locale "%s".', $locale));
    }

    public function getPage(): PageInterface
    {
        return $this->page;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation;

use WS\Library\Page\Model\Translation\PageInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Trait PageRepositoryTrait
 *
 * @author Benjamin Georgeault
 */
trait PageRepositoryTrait
{
    public function findOneBySiteAndHierarchySlug(SiteInterface $siteTranslation, string $hierarchySlug): ?PageInterface
    {
        return $this->createQueryBuilder('pt')
            ->join('pt.translatable', 'p')
            ->where('p.site = :site')
            ->andWhere('pt.hierarchySlug = :hierarchySlug')
            ->setParameter('site', $siteTranslation->getTranslatable())
            ->setParameter('hierarchySlug', $hierarchySlug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}

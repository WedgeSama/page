<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation;

use Doctrine\Persistence\ObjectRepository;
use WS\Library\Page\Model\Translation\PageInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Interface PageRepositoryInterface
 *
 * @author Benjamin Georgeault
 */
interface PageRepositoryInterface extends ObjectRepository
{
    public function findOneBySiteAndHierarchySlug(SiteInterface $siteTranslation, string $hierarchySlug): ?PageInterface;
}

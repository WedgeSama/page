<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation;

use Doctrine\Persistence\ObjectRepository;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Interface SiteRepositoryInterface
 *
 * @author Benjamin Georgeault
 */
interface SiteRepositoryInterface extends ObjectRepository
{
    public function findOneByHostAndLocale(string $host, string $locale): ?SiteInterface;

    /**
     * @return iterable<SiteInterface>
     */
    public function findByHost(string $host): array;

    public function findOneDefaultBySite(SiteInterface $other): ?SiteInterface;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation;

use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Trait SiteRepositoryTrait
 *
 * @author Benjamin Georgeault
 */
trait SiteRepositoryTrait
{
    public function findOneByHostAndLocale(string $host, string $locale): ?SiteInterface
    {
        return $this->findOneBy([
            'host' => $host,
            'locale' => $locale,
        ]);
    }

    public function findByHost(string $host): array
    {
        return $this->findBy([
            'host' => $host,
        ]);
    }

    public function findOneDefaultBySite(SiteInterface $other): ?SiteInterface
    {
        return $this->findOneBy([
            'translatable' => $other->getTranslatable(),
            'default' => true,
        ]);
    }
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository;

use Doctrine\Persistence\ObjectRepository;
use WS\Library\Page\Model\RedirectInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Interface RedirectRepositoryInterface
 *
 * @author Benjamin Georgeault
 */
interface RedirectRepositoryInterface extends ObjectRepository
{
    public function findOneBySiteAndFrom(SiteInterface $site, string $from): ?RedirectInterface;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Repository;

use WS\Library\Page\Model\RedirectInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Trait RedirectRepositoryTrait
 *
 * @author Benjamin Georgeault
 */
trait RedirectRepositoryTrait
{
    public function findOneBySiteAndFrom(SiteInterface $site, string $from): ?RedirectInterface
    {
        return $this->findOneBy([
            'site' => $site,
            'from' => $from,
        ]);
    }
}

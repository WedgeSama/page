<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\ObjectExtensions\Translation\Model\HierarchyTranslationTrait;
use WS\Library\ObjectExtensions\Translation\Model\TranslationTrait;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\AbstractPage as TranslatableAbstractPage;
use WS\Library\Page\Model\Translation\PageInterface;
use WS\Library\Page\Model\Translation\PageTrait;
use WS\Library\Seo\Bridge\Doctrine\ORM\Entity\HaveMetaTagBagTrait;

/**
 * Class AbstractPage
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslatableAbstractPage
 * @template-implements PageInterface<TKey, TValue>
 */
#[ORM\MappedSuperclass]
abstract class AbstractPage implements PageInterface
{
    use PageTrait;
    use TranslationTrait;
    use HierarchyTranslationTrait;
    use HaveMetaTagBagTrait;

    #[ORM\Column(type: 'string', length: 255)]
    protected ?string $title = null;
}

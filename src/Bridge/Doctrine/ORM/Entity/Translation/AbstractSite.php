<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\ObjectExtensions\Translation\Model\TranslationTrait;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\AbstractSite as TranslatableAbstractSite;
use WS\Library\Page\Model\Translation\SiteInterface;
use WS\Library\Page\Model\Translation\SiteTrait;

/**
 * Class AbstractSite
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslatableAbstractSite
 * @template-implements SiteInterface<TKey, TValue>
 */
#[ORM\MappedSuperclass]
abstract class AbstractSite implements SiteInterface
{
    use SiteTrait;
    use TranslationTrait;

    #[ORM\Column(type: 'string', length: 255)]
    protected ?string $host = null;

    #[ORM\Column(type: 'string', length: 255)]
    protected ?string $title = null;

    #[ORM\Column(name: 'is_default', type: 'boolean')]
    protected bool $default = false;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Page\Model\RedirectInterface;
use WS\Library\Page\Model\RedirectTrait;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Class AbstractRedirect
 *
 * @author Benjamin Georgeault
 */
#[ORM\MappedSuperclass]
abstract class AbstractRedirect implements RedirectInterface
{
    use RedirectTrait;

    #[ORM\Column(type: 'string', length: 1023)]
    protected ?string $from = null;

    #[ORM\Column(type: 'string', length: 1023)]
    protected ?string $to = null;

    #[ORM\Column(type: 'integer')]
    protected int $code = 302;

    #[
        ORM\ManyToOne(targetEntity: SiteInterface::class),
        ORM\JoinColumn(nullable: false),
    ]
    protected ?SiteInterface $site = null;

    public function getSite(): ?SiteInterface
    {
        return $this->site;
    }

    public function setSite(?SiteInterface $site): static
    {
        $this->site = $site;
        return $this;
    }
}

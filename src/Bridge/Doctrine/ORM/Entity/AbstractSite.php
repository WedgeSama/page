<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\ObjectExtensions\Bridge\Doctrine\TranslatableTrait;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation\AbstractSite as TranslationAbstractSite;
use WS\Library\Page\Model\SiteInterface;
use WS\Library\Page\Model\SiteTrait;

/**
 * Class AbstractSite
 *
 * @author Benjamin Georgeault
 *
 * @template T of TranslationAbstractSite
 * @template-implements SiteInterface<T>
 */
#[ORM\MappedSuperclass]
abstract class AbstractSite implements SiteInterface
{
    use SiteTrait;
    use TranslatableTrait;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Doctrine\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\ObjectExtensions\Bridge\Doctrine\EditableHierarchyTrait;
use WS\Library\ObjectExtensions\Bridge\Doctrine\TranslatableTrait;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation\AbstractPage as TranslationAbstractPage;
use WS\Library\Page\Model\PageInterface;
use WS\Library\Page\Model\PageTrait;
use WS\Library\Page\Model\SiteInterface;

/**
 * Class AbstractPage
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslationAbstractPage
 * @template-implements PageInterface<TKey, TValue>
 */
#[ORM\MappedSuperclass]
abstract class AbstractPage implements PageInterface
{
    use PageTrait;
    use EditableHierarchyTrait;
    use TranslatableTrait;

    #[
        ORM\ManyToOne(targetEntity: SiteInterface::class),
        ORM\JoinColumn(nullable: false),
    ]
    protected ?SiteInterface $site = null;

    public function getSite(): ?SiteInterface
    {
        return $this->site;
    }

    public function setSite(?SiteInterface $site): static
    {
        $this->site = $site;

        return $this;
    }
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Routing;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface as SymfonyUrlGeneratorInterface;
use Symfony\Component\Routing\RequestContext;
use WS\Library\ObjectExtensions\Translation\Exception\NotFoundTranslationException;
use WS\Library\Page\Model\PageInterface;
use WS\Library\Page\Routing\UrlGeneratorInterface;

/**
 * Class UrlGenerator
 *
 * @author Benjamin Georgeault
 */
readonly class UrlGenerator implements UrlGeneratorInterface
{
    public function __construct(
        private SymfonyUrlGeneratorInterface $urlGenerator,
        private string $frontRouteName = 'ws_page_front_route',
    ) {}

    public function generate(PageInterface $page, ?string $locale = null, array $parameters = [], int $referenceType = self::ABSOLUTE_PATH): string
    {
        $locale = $locale ?? $page->getCurrentLocale();
        $locale = $locale ?? $page->getFallbackLocale();

        if (null === $pageTranslation = $page->getTranslationOrNull($locale)) {
            throw new NotFoundTranslationException($page, $locale);
        }

        // Set context based on SiteTranslation.
        if (null !== $siteTranslation = $pageTranslation->getSiteTranslation()) {
            if (null !== $host = $siteTranslation->getHost()) {
                $currentContext = $this->urlGenerator->getContext();
                $this->urlGenerator->setContext(RequestContext::fromUri($host));
            }
        }

        $url = $this->urlGenerator->generate($this->frontRouteName, array_merge($parameters, [
            '_locale' => $locale,
            'hierarchySlug' => $pageTranslation->getHierarchySlug(),
        ]), $referenceType);

        // Restore previous context if needed.
        if (isset($currentContext)) {
            $this->urlGenerator->setContext($currentContext);
        }

        return $url;
    }
}

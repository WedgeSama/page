<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;
use WS\Library\Page\Model\Translation\PageInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Class RouteEvent
 *
 * @author Benjamin Georgeault
 */
final class RouteEvent extends Event
{
    private bool $responseOverride = false;

    private array $parameters = [];

    public function __construct(
        private readonly SiteInterface $site,
        private readonly PageInterface $page,
        private readonly Request $request,
        private Response $response = new Response(),
    ) {}

    public function getSite(): SiteInterface
    {
        return $this->site;
    }

    public function getPage(): PageInterface
    {
        return $this->page;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function setResponse(Response $response): RouteEvent
    {
        $this->response = $response;
        $this->responseOverride = true;
        $this->stopPropagation();

        return $this;
    }

    public function isResponseOverride(): bool
    {
        return $this->responseOverride;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function addParameter(string $key, mixed $value): RouteEvent
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function addParameters(array $parameters): RouteEvent
    {
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }

    public function setParameters(array $parameters): RouteEvent
    {
        $this->parameters = $parameters;
        return $this;
    }
}

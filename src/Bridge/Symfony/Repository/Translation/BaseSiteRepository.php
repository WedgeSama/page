<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Repository\Translation;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation\AbstractSite;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation\SiteRepositoryInterface;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation\SiteRepositoryTrait;
use WS\Library\Page\Bridge\Symfony\Entity\Translation\BaseSite;

/**
 * Class BaseSiteTranslationRepository
 *
 * @author Benjamin Georgeault
 */
class BaseSiteRepository extends ServiceEntityRepository implements SiteRepositoryInterface
{
    use SiteRepositoryTrait;

    public function __construct(ManagerRegistry $registry, string $className = BaseSite::class)
    {
        if (is_subclass_of($className, AbstractSite::class) === false) {
            throw new \InvalidArgumentException(sprintf(
                'The class "%s" must be a subclass of "%s".',
                $className,
                AbstractSite::class,
            ));
        }

        parent::__construct($registry, $className);
    }
}

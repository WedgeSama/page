<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\AbstractRedirect;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\RedirectRepositoryInterface;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\RedirectRepositoryTrait;
use WS\Library\Page\Bridge\Symfony\Entity\Redirect;

/**
 * Class RedirectRepository
 *
 * @author Benjamin Georgeault
 */
class RedirectRepository extends ServiceEntityRepository implements RedirectRepositoryInterface
{
    use RedirectRepositoryTrait;

    public function __construct(ManagerRegistry $registry, string $className = Redirect::class)
    {
        if (is_subclass_of($className, AbstractRedirect::class) === false) {
            throw new \InvalidArgumentException(sprintf(
                'The class "%s" must be a subclass of "%s".',
                $className,
                AbstractRedirect::class,
            ));
        }

        parent::__construct($registry, $className);
    }
}

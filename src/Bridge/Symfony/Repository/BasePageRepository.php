<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\AbstractPage;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\PageRepositoryInterface;
use WS\Library\Page\Bridge\Symfony\Entity\BasePage;

/**
 * Class BasePageRepository
 *
 * @author Benjamin Georgeault
 */
class BasePageRepository extends ServiceEntityRepository implements PageRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $className = BasePage::class)
    {
        if (is_subclass_of($className, AbstractPage::class) === false) {
            throw new \InvalidArgumentException(sprintf(
                'The class "%s" must be a subclass of "%s".',
                $className,
                AbstractPage::class,
            ));
        }

        parent::__construct($registry, $className);
    }
}

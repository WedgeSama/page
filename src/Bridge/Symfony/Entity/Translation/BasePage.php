<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Entity\Translation;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation\AbstractPage;
use WS\Library\Page\Bridge\Symfony\Repository\Translation\BasePageRepository;

/**
 * Class BasePage
 *
 * @author Benjamin Georgeault
 */
#[
    ORM\Entity(repositoryClass: BasePageRepository::class),
    ORM\Table(name: 'page'),
    ORM\InheritanceType('JOINED'),
]
abstract class BasePage extends AbstractPage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public static function getSlugFields(): array
    {
        return [
            'title',
        ];
    }
}

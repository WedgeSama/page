<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Entity\Translation;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\Translation\AbstractSite;
use WS\Library\Page\Bridge\Symfony\Repository\Translation\BaseSiteRepository;

/**
 * Class BaseSiteTranslation
 *
 * @author Benjamin Georgeault
 */
#[
    ORM\Entity(repositoryClass: BaseSiteRepository::class),
    ORM\Table(name: 'site'),
    ORM\InheritanceType('SINGLE_TABLE'),
]
abstract class BaseSite extends AbstractSite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}

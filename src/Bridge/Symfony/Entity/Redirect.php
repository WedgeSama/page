<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Page\Bridge\Doctrine\ORM\Entity\AbstractRedirect;
use WS\Library\Page\Bridge\Symfony\Repository\RedirectRepository;

/**
 * Class Redirect
 *
 * @author Benjamin Georgeault
 */
#[ORM\Entity(repositoryClass: RedirectRepository::class)]
class Redirect extends AbstractRedirect
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function getId(): ?int
    {
        return $this->id;
    }
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use WS\Library\Page\Bridge\Symfony\DependencyInjection\Compiler\DefaultEntitiesPass;

/**
 * Class WSPageBundle
 *
 * @author Benjamin Georgeault
 */
class WSPageBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(new DefaultEntitiesPass());
            
            $container->addCompilerPass(DoctrineOrmMappingsPass::createAttributeMappingDriver([
                'WS\Library\Page\Bridge\Doctrine\ORM\Entity',
            ], [
                realpath(__DIR__ . '/../Doctrine/ORM/Entity'),
            ],
                reportFieldsWhereDeclared: true,
            ));

            $container->addCompilerPass(DoctrineOrmMappingsPass::createAttributeMappingDriver([
                'WS\Library\Page\Bridge\Symfony\Entity',
            ], [
                realpath(__DIR__ . '/Entity'),
            ],
                enabledParameter: 'ws_page.default_entities',
                reportFieldsWhereDeclared: true,
            ));
        }
    }
}

<?php
/*
 * This file is part of the lifiachan package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\DependencyInjection\Compiler;

use Doctrine\ORM\Events;
use Doctrine\ORM\Tools\ResolveTargetEntityListener;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use WS\Library\Page\Bridge\Symfony\Entity\BasePage;
use WS\Library\Page\Bridge\Symfony\Entity\BaseSite;
use WS\Library\Page\Bridge\Symfony\Entity\Translation\BasePage as TranslationBasePage;
use WS\Library\Page\Bridge\Symfony\Entity\Translation\BaseSite as TranslationBaseSite;
use WS\Library\Page\Model\PageInterface;
use WS\Library\Page\Model\SiteInterface;
use WS\Library\Page\Model\Translation\PageInterface as TranslationPageInterface;
use WS\Library\Page\Model\Translation\SiteInterface as TranslationSiteInterface;

/**
 * Class DefaultEntitiesPass
 *
 * @author Benjamin Georgeault
 */
class DefaultEntitiesPass implements CompilerPassInterface
{
    private static array $resolves = [
        PageInterface::class => BasePage::class,
        SiteInterface::class => BaseSite::class,
        TranslationPageInterface::class => TranslationBasePage::class,
        TranslationSiteInterface::class => TranslationBaseSite::class,
    ];
    
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition('ws_page.default_entities.resolve_target_entity')) {
            return;
        }

        $definition = $container->getDefinition('ws_page.default_entities.resolve_target_entity');
        
        foreach (self::$resolves as $interface => $class) {
            $definition->addMethodCall('addResolveTargetEntity', [
                $interface,
                $class,
                [],
            ]);
        }
    }
}

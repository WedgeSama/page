<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('ws_page');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->defaultConfigNode())
            ->end()
        ;

        return $treeBuilder;
    }

    private function defaultConfigNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('default');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->booleanNode('base_entities')
                    ->defaultTrue()
                ->end()
                ->booleanNode('controllers')
                    ->defaultTrue()
                ->end()
            ->end()
        ;

        return $node;
    }
}

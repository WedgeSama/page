<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Controller\Front;

use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\RedirectRepositoryInterface;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation\PageRepositoryInterface;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation\SiteRepositoryInterface;
use WS\Library\Page\Bridge\Symfony\Event\RouteEvent;

/**
 * Class RouteAction
 *
 * @author Benjamin Georgeault
 */
final readonly class RouteAction
{
    public function __construct(
        private SiteRepositoryInterface $siteRepository,
        private RedirectRepositoryInterface $redirectRepository,
        private PageRepositoryInterface $pageRepository,
        private Environment $twig,
        private EventDispatcherInterface $dispatcher,
    ) {}

    #[Route('/{_locale}/{hierarchySlug}', name: 'ws_page_front_route', requirements: [
        '_locale' => '^[a-z]{2}$',
        'hierarchySlug' => '.*',
    ])]
    public function __invoke(Request $request, string $hierarchySlug, string $_locale): Response
    {
        if (null === $site = $this->siteRepository->findOneByHostAndLocale($host = $request->getHost(), $_locale)) {
            throw new NotFoundHttpException(sprintf(
                'No site translation found for host "%s" and locale "%s".',
                $host,
                $_locale,
            ));
        }

        if (null !== $redirect = $this->redirectRepository->findOneBySiteAndFrom($site, $hierarchySlug)) {
            return new RedirectResponse(
                $redirect->getTo(),
                $redirect->getCode(),
            );
        }

        if (null === $page = $this->pageRepository->findOneBySiteAndHierarchySlug($site, $hierarchySlug)) {
            throw new NotFoundHttpException(sprintf(
                'No page translation found for site "%s", locale "%s" and hierarchy slug "%s".',
                $site->getTitle(),
                $_locale,
                $hierarchySlug,
            ));
        }

        /** @var RouteEvent $event */
        $event = $this->dispatcher->dispatch(new RouteEvent($site, $page, $request));
        $response = $event->getResponse();

        if ($event->isResponseOverride()) {
            return $response;
        }

        $pageTranslatable = $page->getTranslatable();

        $response->setContent(
            $this->twig->render($pageTranslatable->getTemplate(), array_merge($event->getParameters(), [
                'page_translatable' => $pageTranslatable,
                'page' => $page,
                'site_translatable' => $site->getTranslatable(),
                'site' => $site,
            ])),
        );

        return $response;
    }
}

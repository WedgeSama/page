<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Bridge\Symfony\Controller\Front;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use WS\Library\Page\Bridge\Doctrine\ORM\Repository\Translation\SiteRepositoryInterface;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Class RootAction
 *
 * @author Benjamin Georgeault
 */
final readonly class RootAction
{
    public function __construct(
        private SiteRepositoryInterface $siteRepository,
    ) {}

    #[Route('/', name: 'ws_page_front_root_redirect')]
    public function __invoke(Request $request): RedirectResponse
    {
        if (empty($sites = $this->siteRepository->findByHost($request->getHost()))) {
            throw new NotFoundHttpException(sprintf('Cannot find site for host "%s".', $request->getHost()));
        }

        if (
            (null !== $defaultSite = $this->siteRepository->findOneDefaultBySite($sites[0]))
            && !in_array($defaultSite, $sites)
        ) {
            array_unshift($sites, $defaultSite);
        }

        $locale = $request->getPreferredLanguage(array_map(function (SiteInterface $site) {
            return $site->getLocale();
        }, $sites));

        return new RedirectResponse(sprintf(
            '%s/%s/',
            rtrim($request->getSchemeAndHttpHost(), '/'),
            $locale,
        ));
    }
}

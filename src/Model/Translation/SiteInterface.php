<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model\Translation;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslationInterface;
use WS\Library\Page\Model\SiteInterface as TranslatableSiteInterface;

/**
 * Interface SiteInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslatableSiteInterface
 * @template-extends TranslationInterface<TValue>
 */
interface SiteInterface extends TranslationInterface, CanDisableInterface
{
    public function getId(): mixed;

    public function getHost(): ?string;

    public function setHost(?string $host): static;

    public function getTitle(): ?string;

    public function setTitle(?string $title): static;

    public function isDefault(): bool;

    public function setDefault(bool $default): static;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model\Translation;

use WS\Library\ObjectExtensions\Blame\Model\BlameTrait;
use WS\Library\ObjectExtensions\Disable\Model\CanDisableTrait;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugTrait;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampTrait;

/**
 * Trait PageTrait
 *
 * @author Benjamin Georgeault
 */
trait PageTrait
{
    use HierarchySlugTrait;
    use TimestampTrait;
    use BlameTrait;
    use CanDisableTrait;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getSiteTranslation(): ?SiteInterface
    {
        return $this->getTranslatable()
            ?->getSite()
            ->getTranslationOrNull()
        ;
    }
}

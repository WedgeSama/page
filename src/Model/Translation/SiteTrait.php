<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model\Translation;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableTrait;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugTrait;

/**
 * Trait SiteTrait
 *
 * @author Benjamin Georgeault
 */
trait SiteTrait
{
    use CanDisableTrait;
    use HierarchySlugTrait;

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): static
    {
        $this->host = $host;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function isDefault(): bool
    {
        return $this->default;
    }

    public function setDefault(bool $default): static
    {
        $this->default = $default;

        return $this;
    }
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model\Translation;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;
use WS\Library\ObjectExtensions\Slug\Model\HierarchySlugInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslationInterface;
use WS\Library\Page\Model\PageInterface as TranslatablePageInterface;
use WS\Library\Seo\MetaTags\Model\HaveMetaTagBagInterface;

/**
 * Interface PageInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslatablePageInterface
 * @template-extends TranslationInterface<TValue>
 * @template-extends HierarchySlugInterface<TKey, TValue>
 */
interface PageInterface extends
    HierarchySlugInterface,
    TranslationInterface,
    TimestampInterface,
    CanDisableInterface,
    HaveMetaTagBagInterface
{
    public function getId(): mixed;

    public function getTitle(): ?string;

    public function setTitle(?string $title): static;

    public function getSiteTranslation(): ?SiteInterface;
}

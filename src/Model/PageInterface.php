<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;
use WS\Library\ObjectExtensions\Hierarchy\Model\EditableHierarchyInterface;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;
use WS\Library\Page\Model\Translation\PageInterface as TranslationPageInterface;

/**
 * Interface PageInterface
 *
 * @author Benjamin Georgeault
 *
 * @template TKey
 * @template-covariant TValue of TranslationPageInterface
 * @template-extends TranslatableInterface<TValue>
 * @template-extends EditableHierarchyInterface<TKey, TValue>
 */
interface PageInterface extends EditableHierarchyInterface, TranslatableInterface, TimestampInterface, CanDisableInterface
{
    public function getId(): mixed;

    public function getSite(): ?SiteInterface;

    public function setSite(?SiteInterface $site): static;

    public function getTemplate(): string;
}

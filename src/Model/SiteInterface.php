<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;
use WS\Library\ObjectExtensions\Translation\Model\TranslatableInterface;
use WS\Library\Page\Model\Translation\SiteInterface as TranslationSiteInterface;

/**
 * Interface SiteInterface
 *
 * @author Benjamin Georgeault
 *
 * @template T of TranslationSiteInterface
 * @template-extends TranslatableInterface<T>
 */
interface SiteInterface extends TranslatableInterface, CanDisableInterface
{
    public function getId(): mixed;

    public function getTitle(): ?string;

    public function getBaseTemplate(): string;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableTrait;
use WS\Library\Page\Model\Translation\SiteInterface;

/**
 * Trait SiteTrait
 *
 * @author Benjamin Georgeault
 */
trait SiteTrait
{
    use CanDisableTrait;

    public function getTitle(): ?string
    {
        /** @var SiteInterface $translation */
        if (null === $translation = $this->getTranslationOrNull($this->getFallbackLocale())) {
            foreach ($this->getTranslations() as $translation) {
                return $translation->getTitle();
            }

            return null;
        }

        return $translation->getTitle();
    }
}

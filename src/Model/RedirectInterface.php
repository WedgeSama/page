<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model;

use WS\Library\ObjectExtensions\Disable\Model\CanDisableInterface;

/**
 * Interface RedirectInterface
 *
 * @author Benjamin Georgeault
 */
interface RedirectInterface extends CanDisableInterface
{
    public function getId(): mixed;

    public function getFrom(): ?string;

    public function getTo(): ?string;

    public function getCode(): int;

    public function getSite(): ?Translation\SiteInterface;

    public function setSite(?Translation\SiteInterface $site): static;
}

<?php
/*
 * This file is part of the page package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Page\Model;

use WS\Library\ObjectExtensions\Blame\Model\BlameTrait;
use WS\Library\ObjectExtensions\Disable\Model\CanDisableTrait;
use WS\Library\ObjectExtensions\Timestamp\Model\TimestampTrait;

/**
 * Trait PageTrait
 *
 * @author Benjamin Georgeault
 */
trait PageTrait
{
    use TimestampTrait;
    use BlameTrait;
    use CanDisableTrait;
}
